#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import requests


proxies = open('chek_prox.txt', 'r').read().split('\n')
url = 'https://stackoverflow.com'

for prox in proxies:
    proxy = {'https': prox}
    st = time.time()
    try:
        ans = requests.get(url, proxies=proxy, timeout=0.2)
        print(prox)
    except:
        continue
