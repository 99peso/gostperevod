#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import csv
import threading
from queue import Queue
from settings import *
import requests
from bs4 import BeautifulSoup as BS
import time
import random


class First(threading.Thread):
    def __init__(self, queue, out, prox, good):
        threading.Thread.__init__(self)
        self.queue = queue
        self.out = out
        self.retries = 0
        self.prox = prox
        self.good_prox = good

    def run(self):
        while True:
            url = self.queue.get()
            try:
                self.out.put(self.download_data(url))
            except:
                print('ERRRR')
            self.queue.task_done()

    def add_good(self, prox):
        if prox not in self.good_prox:
            self.good_prox.append(prox)

    def rem_bad(self, prox):
        if prox in self.prox:
            self.prox.remove(prox)

    def download_data(self, url):
        out = []
        while True:
            if not self.good_prox:
                prox = random.choice(self.prox)
            else:
                prox = random.choice(self.good_prox)

            try:
                soup = get_soup(url[0], prox)
                print(prox, 'good', url)
                self.add_good(prox)
                break
            except:
                self.rem_bad(prox)
                print(prox, 'bad')

        try:
            soup = soup.findAll('ol')[1]
        except:
            return out

        lilist = soup.findAll('li')
        for li in lilist:
            tp = li.text.split('(')[0].strip()
            lentp = int(li.text.split('(')[-1].replace(')', '').strip())
            out.append((li.a['href'], url[-1], tp, lentp))
        return out


class Parser(threading.Thread):
    def __init__(self, queue, out, stime, prox, good, bad):
        threading.Thread.__init__(self)

        self.queue = queue
        self.out = out
        self.retries = 0
        self.stime = stime
        self.agents = self.get_agents()
        self.headers = HEADERS
        self.prox = prox
        self.good_prox = good
        self.bad_prox = bad
        self.prox_timer = 0

    def get_agents(self):
        return open('agents.txt', 'r').read().split('\n')

    def get_data(self, url, prox):
        self.headers['User-Agent'] = random.choice(self.agents)
        prox = {'https': prox}
        return requests.get(url, headers=self.headers, proxies=prox).content

    def get_soup(self, url, prox):
        return BS(self.get_data(url, prox), 'lxml')

    def run(self):
        while True:
            url = self.queue.get()
            try:
                self.download_data(url)
            except:
                print('------------------errrrrr', url)

            self.queue.task_done()

    def clean(self, p, span):
        return p.text.replace(span, '').replace('\n', ' ').strip()

    def get_prices(self, soup):
        try:
            price = soup.find('div', {'class': 'price'})['data-price']
            ru_opt = soup.find('input', {'id': "opt_1"})['data-price']
            eng_opt = soup.find('input', {'id': "opt_2"})['data-price']

            if ru_opt == '0':
                price_eng = str(int(eng_opt) + int(price))
                price_ru = price
            else:
                price_ru = str(int(ru_opt) + int(price))
                price_eng = price
        except:
            price_eng = '0'
            price_ru = '0'

        return price_eng, price_ru

    def get_names(self, soup):
        prenames = soup.find('div', {'class': 'names'}).findAll('p')
        name_eng = ''
        name_ru = ''
        descript_eng = ''
        descript_ru = ''

        for p in prenames:
            span = p.span.text

            if 'Name in English' in span:
                name_eng = self.clean(p, span)
            elif 'Name in Russian:' in span:
                name_ru = self.clean(p, span)
            elif 'Description in English:' in span:
                descript_eng = self.clean(p, span)
            elif 'Description in Russian:' in span:
                descript_ru = self.clean(p, span)
        return name_eng, name_ru, descript_eng, descript_ru

    def get_attr(self, soup):
        preattr = soup.find('div', {'class': "attributes"}).findAll('div', {'class': 'row'})

        status = ''
        eformat = ''
        pages = ''
        deliv_eng = ''
        deliv_ru = ''
        sku = ''

        for attr in preattr:
            divs = attr.findAll('div')

            if 'Document status:' in divs[0].text:
                status = divs[1].text.strip()
            elif 'Format:' in divs[0].text:
                eformat = divs[1].text.strip()
            elif 'Page count:' in divs[0].text:
                pages = divs[1].text.strip()
            elif 'Delivery time (for English version):' in divs[0].text:
                deliv_eng = divs[1].text.strip()
            elif 'Delivery time (for Russian version):' in divs[0].text:
                deliv_ru = divs[1].text.strip()
            elif 'SKU:' in divs[0].text:
                sku = divs[1].text.strip()
        return status, eformat, pages, deliv_eng, deliv_ru, sku

    def add_good(self, prox):
        if prox not in self.good_prox:
            self.good_prox.append(prox)

    def rem_bad(self, prox):
        if prox not in self.bad_prox:
            self.bad_prox.append(prox)

        if prox in self.prox:
            self.prox.remove(prox)

        if prox in self.good_prox:
            self.good_prox.remove(prox)

    def new_csv(self, text):
        pre = '|' + str(text) + '|'
        return pre

    def write_temp(self, rows, url):
        with open('out.csv', 'ab') as fl:
            fl.write('\n'.encode('utf8'))
            fl.write(rows.encode('utf8'))

        with open('good_pages.txt', 'a') as f:
            f.write('\n' + url)

    def download_data(self, url):

        fullout = []
        nowtime = time.time()
        ps = round((self.out.qsize() / (nowtime - self.stime)), 1)
        print(len(self.good_prox), len(self.prox), ps, self.out.qsize(), url)
        country = url[1]
        category = url[2]
        self.prox_timer = self.prox_timer + 1

        if self.prox_timer % 10 == 0:
            newprox = open('prox.txt', 'r').read().split('\n')
            for prox in newprox:
                if prox not in self.prox:
                    if prox not in self.bad_prox:
                        self.prox.append(prox)

        while True:
            if self.retries < 1 or len(self.good_prox) < 10:
                prox = random.choice(self.prox)
                self.retries = self.retries + 1
            else:
                if self.good_prox:
                    prox = random.choice(self.good_prox)
                    self.retries = 0
                else:
                    prox = random.choice(self.prox)

            try:
                soup = self.get_soup(url[0], prox)
                produrls = soup.findAll('div', {'class': 'item'})
                self.add_good(prox)
                break
            except:
                self.rem_bad(prox)

        for newurl in produrls:
            try:
                outurl = newurl.find('a')['href']

                while True:
                    if self.good_prox:
                        prox = random.choice(self.good_prox)
                    else:
                        prox = random.choice(self.prox)

                    try:
                        soup = self.get_soup(outurl, prox)
                        break

                    except:
                        self.rem_bad(prox)

                name_eng, name_ru, descript_eng, descript_ru = self.get_names(soup)

                status, eformat, pages, deliv_eng, deliv_ru, sku = self.get_attr(soup)

                price_eng, price_ru = self.get_prices(soup)

                out = (country,
                       category,
                       name_eng,
                       name_ru,
                       descript_eng,
                       descript_ru,
                       status,
                       eformat,
                       pages,
                       deliv_eng,
                       deliv_ru,
                       sku,
                       price_eng,
                       price_ru,
                       outurl)

                new_out = []
                for ou in out:
                    new_out.append(self.new_csv(ou))

                self.out.put(out)
                fullout.append(','.join(new_out))
                #print(out)
            except:
                continue

        self.write_temp('\n'.join(fullout), url[0])


def get_data(url, prox):
    prox = {'https': prox}
    return requests.get(url, headers=HEADERS, proxies=prox).content


def get_soup(url, prox):
    return BS(get_data(url, prox), 'lxml')


def generate_urls(pre):
    out = []
    if pre[-1] % 12 == 0:
        end = int(pre[-1] / 12)
    else:
        end = int(pre[-1] / 12) + 1
    for p in range(1, end + 1):
        out.append((pre[0] + '?p=' + str(p), pre[1], pre[2]))
    return out


def write_out(outq):
    fname = 'gost_out.csv'
    fstr = 'Country,Category,Name Eng,Name Ru,Description Eng,Description Ru,Status,Format,Pages,Delivery Eng,Delivery Ru,SKU,Price Eng,Price Ru,Url'
    with open(os.getcwd() + '/out_data/' + fname, 'w') as outfile:
        csvwriter = csv.writer(outfile)
        csvwriter.writerow(fstr.split(','))
        while not outq.empty():
            csvwriter.writerow(outq.get())


def first():
    queue = Queue()

    fout = []
    proxlist = open('prox.txt', 'r').read().split('\n')
    all_proxy = proxlist
    good_prox = []

    while True:
        prox = random.choice(proxlist)
        print(prox)
        try:
            soup = get_soup(FURL, prox)
            good_prox.append(prox)
            break
        except:
            print(prox, '---------errrrrrrrr')
            continue

    alist = soup.findAll('a', {'class': "level-top"})
    print(len(alist))

    for pre in alist:
        fout.append((pre['href'], pre.text.split(' ')[0]))

    outqueue = Queue()

    for thread in range(NUM_THREADS):
        t = First(queue, outqueue, all_proxy, good_prox)
        t.setDaemon(True)
        t.start()

    for pre in fout:
        queue.put(pre)

    queue.join()

    sfout = []
    print('generate')
    while not outqueue.empty():
        for tmp in outqueue.get():
            sfout += generate_urls(tmp)

    print(len(sfout))
    return sfout


def main():
    sfout = []
    last = open('good_pages.txt', 'r').read().split('\n')

    for new in first():
        if new[0] not in last:
            sfout.append(new)

    queue = Queue()

    proxlist = open('prox.txt', 'r').read().split('\n')

    all_proxy = proxlist
    good_prox = []
    bad_prox = []
    outqueue = Queue()
    tstime = time.time()

    for thread in range(NUM_THREADS):
        t = Parser(queue, outqueue, tstime, all_proxy, good_prox, bad_prox)
        t.setDaemon(True)
        t.start()

    for pre in sfout[:]:
        queue.put(pre)

    queue.join()

    write_out(outqueue)


if __name__ == "__main__":
    main()
