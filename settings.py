#!/usr/bin/python
# -*- coding: utf-8 -*-


HEADERS = {'Host': 'gostperevod.com',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20120101 Firefox/29.0'}
FURL = 'https://gostperevod.com/'
NUM_THREADS = 35
MAX_RETRIES = 5